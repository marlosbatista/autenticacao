﻿using Autenticacao.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Autenticacao.Testes
{
    public class WeatherForecastControllerTests
    {
        [Fact]
        public void Get_ComRegistros_RetornaListaComConteudo()
        {
            // Arrange
            var loggerStub = new Mock<ILogger<WeatherForecastController>>();

            var controller = new WeatherForecastController(loggerStub.Object);

            // Act
            var result = controller.Get();

            // Assert
            Assert.NotEmpty(result);
        }

        [Fact]
        public void Get_SemRegistros_RetornaListaVazia()
        {
            // Arrange
            var loggerStub = new Mock<ILogger<WeatherForecastController>>();

            var controller = new WeatherForecastController(loggerStub.Object);

            // Act
            var result = controller.Get();

            // Assert
            Assert.Empty(result);
        }
    }
}
